import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://127.0.0.1:5000",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
})

export default {
  postCalculation(data) {
    return apiClient.post('/calculate',data)
  },
  postUser(userInfo) {
    return apiClient.post('/register', userInfo)
  },
  postLogin(uname,pass) {
    return apiClient.post('/login',{},{auth:{username:uname,password:pass}})
  },
  getUser(user_token) {
    return apiClient.post('/view_users', {}, {headers: {'access_token': user_token}})
  },
  getMeasurement(user_token) {
    return apiClient.post('/view_measurement_types', {}, {headers: {'access_token': user_token}})
  }

}
