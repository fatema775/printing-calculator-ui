import { createRouter, createWebHistory } from "vue-router";
import User from "../views/User.vue";
import SuperAdmin from "../views/SuperAdmin.vue";

const routes = [
  {
    path: "/",
    name: "Calculation",
    component: () =>
      import("../components/Calculation.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import("../components/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import("../components/Register.vue"),
  },
  {
    path: "/user",
    name: "User",
    component: User,
    children: [
      {
        path: "/user/order",
        name: "PlaceOrder",
        component: () =>
          import("../components/PlaceOrder.vue"),
      },
      {
        path: "/user/vieworders",
        name: "ViewOrder",
        component: () =>
          import("../components/ViewOrder.vue"),
      }
    ]
  },
  {
    path: "/super-admin",
    name: "SuperAdmin",
    component: SuperAdmin,
    children: [
      {
        path: "/super-admin/user-info",
        name: "UserInfo",
        component: () =>
          import("../components/UserInfo.vue"),
      },
      {
        path: "/super-admin/measurement-info",
        name: "MeasurementInfo",
        component: () =>
          import("../components/MeasurementInfo.vue"),
      },
      {
        path: "/super-admin/order-info",
        name: "OrderInfo",
        component: () =>
          import("../components/OrderInfo.vue"),
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
